#!/bin/bash
echo "" > 'index.html'
echo "<script src='https://cdn.jsdelivr.net/npm/vue'></script>" >> 'index.html'
for f in *.*; do ( 
    IFS=-
    set -- $f
    echo "<a href='$*'>$*</a></br>" >> 'index.html'
)
done